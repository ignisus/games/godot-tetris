# Godot Tetris

Godot Engine Flatpak [Play Online](https://ignisus.org/gaming)

## Controls:
* Arrow keys (L-R): Move left and right
* Z: Rotate left
* X: Rotate right
* Up Key: Hard Drop
* Down Key: Soft Drop
* CTRL: Hold piece
* ESC: Quit game
