extends Node

const ignis0 = preload("res://spr/ignis0.png")
const ignis1 = preload("res://spr/ignis1.png")
const ignis2 = preload("res://spr/ignis2.png")
const ignis3 = preload("res://spr/ignis3.png")
const ignis4 = preload("res://spr/ignis4.png")
const ignis5 = preload("res://spr/ignis5.png")
const ignis6 = preload("res://spr/ignis6.png")
const ignis7 = preload("res://spr/ignis7.png")

func getTextureForColorIndex(index):
	match (index):
		(0): return ignis0
		(1): return ignis1
		(2): return ignis2
		(3): return ignis3
		(4): return ignis4
		(5): return ignis5
		(6): return ignis6
		(7): return ignis7
