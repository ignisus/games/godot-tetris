extends Node2D

var screenSizeX
var screenSizeY

func _ready():
	screenSizeX = get_viewport().size.x
	screenSizeY = get_viewport().size.y

	var nodo = $UI

	nodo.rect_min_size = Vector2(screenSizeX * 0.5, screenSizeY * 0.5)
	nodo.rect_size = nodo.rect_min_size
	nodo.rect_position = Vector2(screenSizeX * 0.25, screenSizeY * 0.25)
